#include "vetors.h"

Vetors::Vetors(QObject *parent)
    : QAbstractListModel(parent)
{
    mexample();
}

int Vetors::rowCount(const QModelIndex &parent) const
{

    if (parent.isValid())
        return 0;
    else
        return length;

    // FIXME: Implement me!
}

QVariant Vetors::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    int row = index.row();
    if (row >length || row<0){
        return QVariant();
    }

    if (role == 1){
        return  QVariant(allR[row]);
    }
    else if (role == 0){
        return QVariant(leftR[row]);
    }
    return QVariant();
}

bool Vetors::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (data(index, role) != value) {
        qDebug()<<"az inja";
        if (role == 0){
            leftR[index.row()] = value.toInt();
            return true;
        }
        else if (role ==1){
            allR[index.row()] = value.toInt();
            return true;
        }
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

void Vetors::mexample()
{
    leftR.clear();
    allR.clear();
    leftR<<0<<1<<1;
    allR<<9<<3<<6;
    emit dataChanged(index(0),index(length-1),QVector<int>()<<0<<1);

}

void Vetors::mclear()
{
    for(int i =0;i<length;i++){
        allR[i] = 0;
        leftR[i]=0;

    }
    emit dataChanged(index(0),index(length-1),QVector<int>()<<0<<1);

}



Qt::ItemFlags Vetors::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable; // FIXME: Implement me!
}

bool Vetors::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent, row, row + count - 1);
    length++;
    endInsertRows();
    return true;
}

bool Vetors::removeRows(int row, int count, const QModelIndex &parent)
{
    beginRemoveRows(parent, row, row + count - 1);
    length--;
    endRemoveRows();
    return true;
}

QHash<int, QByteArray> Vetors::roleNames() const
{
    QHash<int,QByteArray> roles;
    roles[0] = "lr";
    roles[1] = "ar";
    return roles;
}
