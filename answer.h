#ifndef ANSWER_H
#define ANSWER_H

#include <QAbstractListModel>
#include <QDebug>
#include<QVector>

class Answer : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit Answer(QObject *parent = nullptr);

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    Q_INVOKABLE int setResult(QVector<int> data);
    Q_INVOKABLE void calltest();
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QHash<int,QByteArray> roleNames() const override;
    bool setData(const QModelIndex &index,const QVariant &value,int role = Qt::EditRole) override;
    Qt::ItemFlags flags(const QModelIndex& index) const override;
    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

private:
    QVector<int> path;

public slots:
    void OnAnswerReady(QString Data);

};

#endif // ANSWER_H
