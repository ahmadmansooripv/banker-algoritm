#ifndef TOOLS_H
#define TOOLS_H

#include <QObject>
#include <QVector>

class tools:public QObject
{
    Q_OBJECT
public:
    tools();
    Q_INVOKABLE QVector<int> bridge() const;
    Q_INVOKABLE void setBridge(QVector<int> data);
private:
    QVector<int> data;
};


#endif // TOOLS_H
