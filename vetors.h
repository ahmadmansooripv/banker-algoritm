#ifndef VETORS_H
#define VETORS_H

#include <QAbstractListModel>
#include <QDebug>

class Vetors : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit Vetors(QObject *parent = nullptr);

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;
    Q_INVOKABLE void mexample();
    Q_INVOKABLE void mclear();
    Qt::ItemFlags flags(const QModelIndex& index) const override;

    // Add data:
    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

    // Remove data:
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;

    QHash<int,QByteArray> roleNames() const override;


private:
    qint8 length = 3;
    QVector<int> leftR;
    QVector<int> allR;
};

#endif // VETORS_H
