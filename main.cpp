#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickStyle>
#include<QQmlContext>
#include <vetors.h>
#include <dataprovider.h>
#include <b.h>
#include<answer.h>
#include<tools.h>

int main(int argc, char *argv[])
{
    qmlRegisterType<Vetors>("core.Vectors",1,0,"Vectors");
    qmlRegisterType<DataProvider>("core.DataProvider",1,0,"DataProvider");
    qmlRegisterType<Answer>("core.Answer",1,0,"Answer");
    tools tools;
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);
    QQuickStyle::setStyle("Material");
    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("tools",&tools);
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
