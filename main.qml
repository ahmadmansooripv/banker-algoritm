import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Window 2.12
import QtQuick.Controls.Material 2.3
import core.DataProvider 1.0
import Qt.labs.qmlmodels 1.0
import QtQml.Models 2.3


ApplicationWindow {
    id:mainwindow
    visible: true
    x:100
    y:50
    width: screen.width
    height: screen.height
    title: qsTr("Hello World")
    Material.theme :Material.Dark
    Material.accent: Material.Purple
    Material.background: "#212121"

    DataProvider{
        id:dataprovider
    }
    SwipeView{
        id:swipv
        width: parent.width
        height: parent.height
        currentIndex: 0
        First{
            id:firstpage
        }
        Second{
            id:secondpage

        }
    }


    PageIndicator{
        id:sindicator
        count: swipv.count
        currentIndex: swipv.currentIndex
        anchors.bottom: swipv.bottom
        anchors.horizontalCenter: swipv.horizontalCenter

    }

    function qbridge (){
        secondpage.refresh();
    }

}
