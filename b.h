#ifndef B_H
#define B_H
#include<QDebug>
#include <QAbstractTableModel>

class B : public QAbstractTableModel
{
    Q_OBJECT

public:
    explicit B(QObject *parent = nullptr);
    QHash<int, QByteArray> roleNames() const override;
    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;
    // Add data:
    bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
    bool insertColumns(int column, int count, const QModelIndex &parent = QModelIndex()) override;

    // Remove data:
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override;
    bool removeColumns(int column, int count, const QModelIndex &parent = QModelIndex()) override;
//    Q_INVOKABLE void addRow();

private:
    int numofcolumn =3;
    int numofrow=4;
    mutable QVector<QVector<int>> A;
    mutable QVector<QVector<int>> Claim;
};

#endif // B_H
