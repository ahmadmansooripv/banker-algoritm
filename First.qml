import QtQuick 2.12
import QtQuick.Controls 2.1
import QtQuick.Controls.Material 2.3
import core.DataProvider 1.0
import core.Vectors 1.0
import Qt.labs.qmlmodels 1.0
import QtQml.Models 2.3
import QtQuick.Window 2.12


Item {
    RoundButton{
        id:example
        anchors.topMargin: 20
        text: "example"
        width: parent.width/20
        height: width
        anchors.top:parent.top
        anchors.rightMargin: 10
        anchors.right: solve.left
        onClicked: {

            dataprovider.mexample();
            corevector.mexample();
        }

    }
    RoundButton{
        id:clear
        anchors.verticalCenter: example.verticalCenter
        anchors.right: example.left
        anchors.rightMargin: 10
        text:"clear"
        width: example.width
        height: width
        onClicked: {
            dataprovider.mclear();
            corevector.mclear();
        }
    }
    RoundButton{
        id:solve

        anchors.verticalCenter: example.verticalCenter
        anchors.right: parent.right
        anchors.rightMargin: 20
        text:"»"
        width: example.width
        height: width
        onClicked: {
            tools.setBridge(dataprovider.bankers());
            mainwindow.qbridge();
            swipv.currentIndex = 1;

        }
    }

    RoundButton{
        id:addrows
        anchors.bottom: table.top
        anchors.left:table.left
        width: 50
        height:50
        Label{
            text:"+"
            font.bold:true
            color: "black"
            anchors.centerIn: parent
        }

        Material.background: "yellow"
        onClicked: {
//            dataprovider.addRow();
//            dataprovider2.addRow();
        }
    }
    RoundButton{
        anchors.bottom: table.top
        anchors.left:addrows.right
        width: 50
        height:50
        Material.background: "pink"
        onClicked: {
//            dataprovider.addCol();
            dataprovider2.addCol();
        }
    }

    Loader{ sourceComponent: table }

    TableView{
        id:table
        reuseItems: false
//        anchors.centerIn: parent
        anchors.left: parent.left
        anchors.leftMargin: 30
        anchors.verticalCenter: parent.verticalCenter
        width: parent.width/4
        height: width
        columnSpacing: 10
        rowSpacing: 10
        clip: true
        model: dataprovider

        delegate:TextField{
            text: model.data
            maximumLength: 2
//            implicitWidth: table.width/6
            onEditingFinished: {
                model.data = text;

            }
        }

    }
    TableView{
        id:table2
        reuseItems: false
//        anchors.centerIn: parent
        anchors.left: table.right
        anchors.leftMargin: 60
        anchors.verticalCenter: parent.verticalCenter
        width: parent.width/4
        height: width
        columnSpacing: 10
        rowSpacing: 10
        clip: true
        model:dataprovider

        delegate:TextField{
            text: model.b

            maximumLength: 2
            onEditingFinished: {
                model.b = text;
            }
        }

    }

    ListView{
        id:v
        anchors.left: table2.right
        anchors.leftMargin:  40

        anchors.top: table2.top
        width: parent.width/5
        spacing: 10
        model:Vectors{
            id:corevector
        }

        orientation:ListView.Horizontal
        delegate:TextField{
            width: 30
            text: model.lr

            onEditingFinished: {
                model.lr = text;
                dataprovider.mLeftR(model.index,text);
            }
        }
    }

    ListView{
        anchors.left: v.right
        anchors.verticalCenter: v.top
        width: parent.width/5
        spacing: 10
        model:corevector

        orientation:ListView.Horizontal
        delegate:TextField{
            width: 30
            text:model.ar
            onEditingFinished: {
                model.ar = text
                dataprovider.mAllR(model.index,text);
            }
        }
    }
}
