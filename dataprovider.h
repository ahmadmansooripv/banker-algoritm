#ifndef DATAPROVIDER_H
#define DATAPROVIDER_H


#include <QObject>
#include <QAbstractTableModel>

class DataProvider : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit DataProvider(QObject *parent = nullptr);
    int rowCount(const QModelIndex & = QModelIndex()) const override;
    int columnCount(const QModelIndex & = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    bool setData(const QModelIndex &index,const QVariant &value,int role = Qt::EditRole) override;
    QHash<int, QByteArray> roleNames() const override;
    Q_INVOKABLE void addRow();
    Q_INVOKABLE void addCol();
    Q_INVOKABLE void romoveCol();
    Q_INVOKABLE void removeRow();
    Q_INVOKABLE void mclear();
    Q_INVOKABLE void mexample();
    Q_INVOKABLE void mAllR(QVariant row,QVariant data);
    Q_INVOKABLE void mLeftR(QVariant row,QVariant data);
    Q_INVOKABLE QVector<int> bankers();
    virtual bool insertRows (int position, int rows, const QModelIndex & parent = QModelIndex())override;
    virtual bool insertColumns(int position,int columns,const QModelIndex &parent = QModelIndex()) override;
//    virtual bool removeRows (int position, int rows, const QModelIndex & parent = QModelIndex())override;
signals:
    void answerReady(QString data);
private:

    bool test(QVector<int> available,QVector<int> need,int numofR);

    qint8 numofrows = 4;
    qint8 numofcolumns =3;
    QVector<int> numofLR;
    QVector<int> numofR;
    QVector<QVector<int>> A;
    QVector<int> result;
    QVector<QVector<int>> B_A;
    QVector<QVector<int>> B;



};

#endif // DATAPROVIDER_H
