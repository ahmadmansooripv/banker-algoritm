#include "dataprovider.h"
#include <QDebug>
#include <answer.h>

DataProvider::DataProvider(QObject *parent) : QAbstractTableModel(parent)
{
    mexample();
}

int DataProvider::rowCount(const QModelIndex &) const
{
    return numofrows;
}

int DataProvider::columnCount(const QModelIndex &) const
{
    return numofcolumns;
}

QVariant DataProvider::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    int row = index.row();
    int col = index.column();
    if (row > numofrows || row <0 || col > numofcolumns || col<0)
        return QVariant();

    if (role == 0)
        return QVariant(A[row][col]);
    else if (role ==1)
        return QVariant(B[row][col]);
    else if (role == 2)
    {
        return QVariant(numofLR[col]);
    }
    else if (role == 3){
        return QVariant(numofR[col]);
    }
    else{
        return QVariant(0);
    }
}

bool DataProvider::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (data(index, role) != value) {
        qDebug()<<numofLR;
        int row = index.row();
        int col = index.column();
        if (role == 0){
            A[row][col] = value.toInt();
            emit dataChanged(index, index, QVector<int>() << role);
            return true;
        }
        else if (role == 1){
            B[row][col] = value.toInt();
            emit dataChanged(index,index,QVector<int>()<<role);
            return true;
        }
        else if (role == 2){
            qDebug()<<row<< " i am here "<<endl;
            numofLR[col] = value.toInt();
            emit dataChanged(index,index,QVector<int>()<<role);
            return true;
        }
        else if (role ==3){
            numofR[col] = value.toInt();
            emit dataChanged(index,index,QVector<int>()<<role);
            return true;
        }
    }
    return false;
}

QHash<int, QByteArray> DataProvider::roleNames() const
{
    QHash<int,QByteArray> roles;
    roles[0] = "data";
    roles[1] = "b";
    roles[2] = "leftR";
    roles[3] = "allR";

    return roles;
}


void DataProvider::addRow()
{
    insertRows(0,1,QModelIndex());

}
void DataProvider::addCol()
{
    insertColumns(0,1);
}

void DataProvider::romoveCol()
{
    numofcolumns--;
}
void DataProvider::removeRow()
{
    numofrows--;
}

void DataProvider::mclear()
{
    //     A[0][0] =numofrows;
    //     B[0][0] = numofcolumns;
    for (int i=0;i<numofrows;i++){
        for(int j=0;j<numofcolumns;j++){
            A[i][j] = 0;
            B[i][j] = 0;
        }
    }
    emit dataChanged(index(0,0),index(numofrows-1,numofcolumns-1),QVector<int>()<<0<<1);
}

void DataProvider::mexample()
{
    for(int i=0;i<A.size();i++){
        A[i].clear();
        B[i].clear();
    }

    QVector<int> current;
    QVector<QVector<int>> AA;
    QVector<QVector<int>> BB;
    QVector<int> LeftResources;
    QVector<int> AllResources;

    AllResources<<9<<3<<6;
    LeftResources<<0<<1<<1;

    current<<1<<0<<0;
    AA.append(current);
    current.clear();
    current<<6<<1<<2;
    AA.append(current);
    current.clear();
    current<<2<<1<<1;
    AA.append(current);
    current.clear();
    current<<0<<0<<2;
    AA.append(current);

    current.clear();
    current<<3<<2<<2;
    BB.append(current);
    current.clear();
    current<<6<<1<<3;
    BB.append(current);
    current.clear();
    current<<3<<1<<4;
    BB.append(current);
    current.clear();
    current<<4<<2<<2;
    BB.append(current);


    A = AA;
    B= BB;
    numofR = AllResources;
    numofLR = LeftResources;

    emit dataChanged(index(0,0),index(numofrows-1,numofcolumns-1),QVector<int>()<<0<<1<<2<<3);
}

void DataProvider::mLeftR(QVariant row, QVariant data)
{
    if (row.toInt()<numofcolumns || row.toInt()>0){
        numofLR[row.toInt()] = data.toInt();
    }

}

bool DataProvider::test(QVector<int> available,QVector<int> need,int numOfR){
    for (int i=0;i<numOfR;i++){
        if(need[i] > available[i]){
            return false;
        }
    }
    return true;
}

QVector<int> DataProvider::bankers()
{
    QVector<QVector<int>> B_AA;
    QVector<QVector<int>> AA;
    QVector<QVector<int>> BB;
    QVector<int> leftR;
    QVector<int> AllR;
    leftR = numofLR;
    AA = A;
    BB = B;

    QVector<int> Res;
    bool nextLevel = false;
    QVector<int> current;
    //creating c_a matrix
    for(int i =0;i<4;i++){
        for (int j=0;j<3;j++){
            current.append(BB[i][j] - AA[i][j]);
        }
        B_AA.append(current);
        current.clear();

    }
//    qDebug()<<"SAFE AA is : ";
//    for (auto& b:B_AA){
//        qDebug()<<b<<" ";
//    }
//    B_A = B_AA;
    for (int i=0;i<numofrows;i++){
//        qDebug()<<"this is V : ";
//        for(auto& b:leftR){
//            qDebug()<<b<<" ";
//        }
//        for(auto& c:B_AA){
//            for(auto& b:c){
//                qDebug()<<b<<" ";
//            }
//            qDebug()<<endl;
//        }
//        qDebug()<<endl;
        if (B_AA[i][0]!=0 || B_AA[i][1]!=0 || B_AA[i][2]!=0){

            for(int j=0;j<numofcolumns;j++){
                bool testR = test(leftR,B_AA[i],B_AA[i].size());
                if (testR){
                    Res << i;
                    //                        pathn++;
                    for(int k =0;k<numofcolumns;k++){
                        BB[i][k]   = 0;
                        B_AA[i][k] = 0;
                        leftR[k]  += AA[i][k];
                        BB[i][k]   = 0;
                        nextLevel  = true;
                    }
                    if (nextLevel)
                        i = -1;
                    nextLevel = false;
                    break;
                }
                else{
                    break;
                }

            }
        }
    }
    result = Res;
    qDebug()<<"from banker "<<result;
    if (result.size()==numofrows){

        qDebug()<<endl;
        return result;
    }
        else{
        qDebug()<<" your path is unSAFE \n";
        return QVector<int>();
    }
}

void DataProvider::mAllR(QVariant row,QVariant data)
{
    if (row.toInt()<numofcolumns || row.toInt()>0){
        numofR[row.toInt()] = data.toInt();
    }
}


bool DataProvider::insertRows(int position, int rows, const QModelIndex &parent)
{
    Q_UNUSED(parent)
    beginInsertRows(QModelIndex(),position,position+rows+1);
    numofrows++;
    endInsertRows();
    return true;
}

bool DataProvider::insertColumns(int position, int columns, const QModelIndex &parent)
{
    Q_UNUSED(parent)
    beginInsertColumns(QModelIndex(),position,position+columns+1);
    numofcolumns++;
    endInsertColumns();
    return true;

}


