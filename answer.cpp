#include "answer.h"

Answer::Answer(QObject *parent)
    : QAbstractListModel(parent)
{
//    path<<2<<3<<4<<5;
}

int Answer::rowCount(const QModelIndex &parent) const
{
    if (!parent.isValid()){
        return path.size();

    }
    return path.size();
}

Qt::ItemFlags Answer::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;
    return Qt::ItemIsEditable; // FIXME: Implement me!
}

bool Answer::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent, row, row + count - 1);

    endInsertRows();
    return true;
}

QVariant Answer::data(const QModelIndex &index, int role) const
{

    if (!index.isValid())
        return QVariant();
    if (index.row() > path.size() || index.row() < 0)
        return QVariant();
    if (role == 0){
        return path[index.row()];
    }

    return QVariant(0);
}

QHash<int, QByteArray> Answer::roleNames() const
{
    QHash<int,QByteArray> roles;
    roles[0] = "data";
    return roles;
}

bool Answer::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (data(index, role) != value) {
        path[index.row()] = value.toInt();
        return true;
    }
    else{
        return  false;
    }
}

void Answer::OnAnswerReady(QString Data)
{
    qDebug()<<"from answer "<<Data;
}
int Answer::setResult(QVector<int> data)
{
    path.clear();
    for (auto& a:data)
        path<<a;
//    qDebug()<<path.size();
//    qDebug()<<"from second page"<<path;
//    emit dataChanged(index(0),index(path.size()-1),QVector<int>()<<0);
//    emit dataChanged(index(0),index(2),QVector<int>()<<0);
    beginResetModel();
    endResetModel();
    return path.size();
}

void Answer::calltest()
{
 qDebug()<<"form call test "<<path.size();
 emit dataChanged(index(0),index(path.size()-1),QVector<int>()<<0);
}
