#include "b.h"

B::B(QObject *parent)
    : QAbstractTableModel(parent)
{

    QVector<int> current;
    current<<3<<2<<2;
    Claim.append(current);
    current.clear();
    current<<6<<1<<3;
    Claim.append(current);
    current.clear();
    current<<3<<1<<4;
    Claim.append(current);
    current.clear();
    current<<4<<2<<2;
    Claim.append(current);
}


QHash<int, QByteArray> B::roleNames() const
{
    QHash<int,QByteArray> names;
    names[0] = "data";
    return names;
}


int B::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return numofrow;

  return numofrow;
}

int B::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return numofcolumn;

    return numofcolumn;
}

QVariant B::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    int row = index.row();
    int col = index.column();
    qDebug()<<"hadfasdf";
    if (row > numofrow || row <0 || col > numofcolumn || col<0)
        return QVariant();

    if (role == 0)
        return QVariant(Claim[row][col]);
    else
        return QVariant(0);


}

bool B::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (data(index, role) != value) {
        int row = index.row();
        int col = index.column();
        Claim[row][col] = value.toInt();
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

//Qt::ItemFlags B::flags(const QModelIndex &index) const
//{
//    if (!index.isValid())
//        return Qt::NoItemFlags;

//    return Qt::ItemIsEditable; // FIXME: Implement me!
//}

bool B::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent, row, row + count - 1);
    numofrow++;
    endInsertRows();
    return true;
}

bool B::insertColumns(int column, int count, const QModelIndex &parent)
{

    beginInsertColumns(parent, column, column + count - 1);
    numofcolumn++;
    endInsertColumns();
    return true;
}

bool B::removeRows(int row, int count, const QModelIndex &parent)
{
    beginRemoveRows(parent, row, row + count - 1);
        numofrow--;
    endRemoveRows();
        return true;
}

bool B::removeColumns(int column, int count, const QModelIndex &parent)
{
    beginRemoveColumns(parent, column, column + count - 1);
    numofcolumn--;
    endRemoveColumns();
        return true;
}

